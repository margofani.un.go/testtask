package restaurant.endpoint.dto;

import lombok.*;

import java.util.List;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
//проблема
public class CookOrderReqDto {
    private String type;
    private String status;
    private String note;
    private Long user;
}
