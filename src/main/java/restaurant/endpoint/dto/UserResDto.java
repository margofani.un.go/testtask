package restaurant.endpoint.dto;

import lombok.*;
import restaurant.entity.Role;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResDto {
    private Long id;
    private String surname;
    private String name;
    private String login;
    private String password;
    private Role stuff;
    private List<Long> orders;
    private List<Long> activeOrders;
    private List<Long> doneOrders;
}
