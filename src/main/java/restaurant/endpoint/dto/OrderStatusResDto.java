package restaurant.endpoint.dto;

import lombok.*;
import restaurant.entity.CookOrder;
import restaurant.entity.Status;
import restaurant.entity.User;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderStatusResDto {
    private Long id;
    private Long order;
    private Status status;
    private IdNameDto user;
    private LocalDateTime time;
}
