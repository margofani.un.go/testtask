package restaurant.endpoint.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserReqDto {
    private String surname;
    private String name;
    private String login;
    private String password;
    private String stuff;
}
