package restaurant.endpoint.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductOrderReqDto {
    private String product;
    private String howMuch;
    private Long order;
}
