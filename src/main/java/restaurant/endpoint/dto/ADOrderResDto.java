package restaurant.endpoint.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ADOrderResDto {
    private Long id;
    private IdNameDto user;
    private Long orderId;
}
