package restaurant.endpoint.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ADOrderReqDto {
    private Long userId;
    private Long orderId;
}
