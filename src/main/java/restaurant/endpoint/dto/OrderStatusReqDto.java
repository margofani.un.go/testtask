package restaurant.endpoint.dto;

import lombok.*;
import restaurant.entity.Status;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderStatusReqDto {
    private Long order;
    private String status;
    private Long user;

}
