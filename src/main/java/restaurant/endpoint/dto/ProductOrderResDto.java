package restaurant.endpoint.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductOrderResDto {
    private Long id;
    private String product;
    private String howMuch;
    private Long order;
}
