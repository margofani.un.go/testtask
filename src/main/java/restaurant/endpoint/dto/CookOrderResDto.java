package restaurant.endpoint.dto;

import lombok.*;
import restaurant.entity.OrderType;
import restaurant.entity.Status;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CookOrderResDto {
    private Long id;
    private OrderType type;
    private List<String> products;
    private String note;
    private IdNameDto user;
    private Status status;
}
