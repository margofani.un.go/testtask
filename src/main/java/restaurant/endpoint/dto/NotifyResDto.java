package restaurant.endpoint.dto;

import lombok.*;

import java.util.function.BinaryOperator;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotifyResDto {
    private Long id;
    private Long orderId;
    private IdNameDto performer;
    private IdNameDto recipient;
    private Boolean status;
    private String message;


}
