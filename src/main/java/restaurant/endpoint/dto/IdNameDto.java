package restaurant.endpoint.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdNameDto {
    private Long id;
    private String name;
}
