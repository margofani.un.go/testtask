package restaurant.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAuthorizationReqDto {
    private String login;
    private String password;

}
