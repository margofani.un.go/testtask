package restaurant.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import restaurant.endpoint.dto.CookOrderReqDto;
import restaurant.endpoint.dto.CookOrderResDto;
import restaurant.endpoint.dto.OrderStatusReqDto;
import restaurant.entity.Status;
import restaurant.representation.CookOrderRepService;
import restaurant.representation.OrderStatusRepService;

import java.util.List;

@RestController
@RequestMapping("/order")
public class CookOrderController {

    private final CookOrderRepService cookOrderRepService;
    private final OrderStatusRepService orderStatusRepService;

    @Autowired
    public CookOrderController(CookOrderRepService cookOrderRepService, OrderStatusRepService orderStatusRepService) {
        this.cookOrderRepService = cookOrderRepService;
        this.orderStatusRepService = orderStatusRepService;
    }

    //работает
    //через этот запрос выводить открытые заказы для снабженцев и кладовщиков
    @GetMapping
    public List<CookOrderResDto> getAll(@RequestParam(name = "status", required = false) List<String> status,
                                        @RequestParam(name = "type", required = false) String type) {
        return cookOrderRepService.getAll(status, type);
    }

    //работает
    @GetMapping("/{id}")
    public CookOrderResDto getById(@PathVariable("id") Long id) {
        return cookOrderRepService.getById(id);
    }

    //работает
    @PostMapping("/create/now")
    public CookOrderResDto createNow(@RequestHeader(value = "user-id") Long userId) {
        return cookOrderRepService.createNow(userId);

    }

    //работает
    @PostMapping("/create/then")
    public CookOrderResDto createThen(@RequestHeader(value = "user-id") Long userId) {
        return cookOrderRepService.createThen(userId);

    }

    //работает
    @PatchMapping("/{id}")
    public CookOrderResDto update(@RequestHeader(value = "user-id") Long userId,
                                  @RequestBody CookOrderReqDto cookOrderReqDto,
                                  @PathVariable("id") Long id) {
        return cookOrderRepService.update(cookOrderReqDto, id, userId);
    }

    //работает
    @DeleteMapping("/{id}")
    public Boolean deleteById(@RequestHeader(value = "user-id") Long userId,
                              @PathVariable("id") Long id) {
        return cookOrderRepService.deleteById(id, userId);
    }

    //работает
    @PostMapping("/repeat/{id}")
    public CookOrderResDto repeat(@PathVariable("id") Long id,
                                  @RequestHeader(value = "user-id") Long userId) {
        return cookOrderRepService.repeat(id, userId);
    }




}
