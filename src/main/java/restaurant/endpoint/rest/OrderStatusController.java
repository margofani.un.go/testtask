package restaurant.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import restaurant.endpoint.dto.OrderStatusReqDto;
import restaurant.endpoint.dto.OrderStatusResDto;
import restaurant.representation.OrderStatusRepService;

import java.util.List;

@RestController
@RequestMapping("/status")
public class OrderStatusController {

    private final OrderStatusRepService orderStatusRepService;

    @Autowired
    public OrderStatusController(OrderStatusRepService orderStatusRepService) {
        this.orderStatusRepService = orderStatusRepService;
    }

    //работает
    @GetMapping
    public List<OrderStatusResDto> getAll(@RequestParam(name = "status", required = false) List<String> status) {
        return orderStatusRepService.getAll(status);
    }

    //работает
    @GetMapping("/{id}")
    public OrderStatusResDto getById(@PathVariable("id") Long id) {
        return orderStatusRepService.getById(id);
    }

    //работает
    @PostMapping
    public OrderStatusResDto create(@RequestBody OrderStatusReqDto orderStatusReqDto) {
        return orderStatusRepService.create(orderStatusReqDto);
    }

    //работает
    @PatchMapping("/{id}")
    public OrderStatusResDto update(@RequestBody OrderStatusReqDto orderStatusReqDto, @PathVariable("id") Long id) {
        return orderStatusRepService.update(orderStatusReqDto, id);
    }

    //работает
    @DeleteMapping("/{id}")
    public Boolean deleteById(@PathVariable("id") Long id) {
        return orderStatusRepService.deleteById(id);
    }

}
