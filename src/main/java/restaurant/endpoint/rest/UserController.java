package restaurant.endpoint.rest;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import restaurant.endpoint.dto.*;
import restaurant.entity.ActiveOrder;
import restaurant.entity.DoneOrder;
import restaurant.representation.impl.UserRepServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserRepServiceImpl userRepService;

    @Autowired
    public UserController(UserRepServiceImpl userRepService) {
        this.userRepService = userRepService;
    }

    //работает
    @GetMapping
    public List<IdNameDto> getAll(@RequestParam(name = "role", required = false) List<String> role) {
        return userRepService.getAll(role);
    }

    //работает
    @GetMapping("/login/{login}")
    public UserResDto getByLogin(@PathVariable("login") String login) {
        return userRepService.getByLogin(login);
    }

    //работает
    @GetMapping("/{id}")
    public UserResDto getById(@PathVariable("id") Long id) {
        return userRepService.getById(id);
    }

    //работает
    @GetMapping("/suggest")
    public List<IdNameDto> getAllByStubName(@RequestParam(value = "name", required = false) String name,
                                            @RequestParam(value = "sName", required = false) String sName) {
        return userRepService.getAllByName(name, sName);
    }

    //работает
    @PostMapping("/registration")
    public UserResDto create(@RequestHeader(value = "user-id") Long userId,
                             @RequestBody UserReqDto userReqDto) {
        return userRepService.create(userReqDto, userId);
    }

    //работает
    @PatchMapping("/{id}")
    public UserResDto updateById(@RequestHeader(value = "user-id") Long userId,
                                 @PathVariable("id") Long id,
                                 @RequestBody UserReqDto userReqDto) {
        return userRepService.update(userReqDto, id, userId);
    }

    //работает
    @DeleteMapping("/{id}")
    public Boolean deleteById(@RequestHeader(value = "user-id") Long userId,
                              @PathVariable("id") Long id) {
        return userRepService.deleteById(id, userId);
    }

    //работает
    @PostMapping("/authorization")
    public UserResDto authorization(@RequestBody UserAuthorizationReqDto authorizationBody) {
        return userRepService.authorization(authorizationBody);
    }

    //работает
    //вывод заказов по статусу
    //через этот запрос повар может посмотреть какой заказ на каком этапе
    @GetMapping("/orders")
    public List<CookOrderResDto> getOrdersByStutus(@RequestHeader(value = "user-id") Long userId,
                                                   @RequestParam(name = "status", required = false) String status) {
        return userRepService.getOrdersByStatus(status, userId);
    }

    //работает
    //вывод активных заказов
    //предназначено для поваров для отслеживания заказов
    @GetMapping("/activeOrders")
    public List<CookOrderResDto> getOrders(@RequestHeader(value = "user-id") Long userId) {
        return userRepService.getOrders(userId);
    }

    //работает
    @PostMapping("/add/active")
    public ADOrderResDto addActive(@RequestHeader(value = "user-id") Long userId,
                                   @RequestBody ADOrderReqDto adOrderDto) {
        return userRepService.addActive(adOrderDto, userId);
    }

    //работает
    @DeleteMapping("/active/{id}")
    public Boolean deleteActive(@RequestHeader(value = "user-id") Long userId,
                                @PathVariable("id") Long id) {
        return userRepService.deleteActive(id, userId);
    }

    //работает
    @PostMapping("/add/done")
    public ADOrderResDto addDone(@RequestHeader(value = "user-id") Long userId,
                                 @RequestBody ADOrderReqDto adOrderDto) {
        return userRepService.addDone(adOrderDto, userId);
    }

    //работает
    @DeleteMapping("/done/{id}")
    public Boolean deleteDone(@RequestHeader(value = "user-id") Long userId,
                              @PathVariable("id") Long id) {
        return userRepService.deleteDone(id, userId);
    }

    //работает
    @GetMapping("/notify")
    public List<NotifyResDto> getNotify(@RequestParam(name = "recipientId", required = false) Long recipientId) {
        return userRepService.getNotify(recipientId);
    }

    //работает
    @GetMapping("/notify/{id}")
    public NotifyResDto getNotifyById(@PathVariable("id") Long id) {
        return userRepService.getNotifyById(id);
    }

    //работает
    @PatchMapping("/notify/status/{id}")
    public Boolean setNotifyStatus(@PathVariable("id") Long notifyId) {
        return userRepService.setNotifyStatus(notifyId);
    }
    //работает
    @GetMapping("/active")
    public List<ADOrderResDto> getActive(@RequestHeader(value = "user-id") Long userId) {
        return userRepService.getActive(userId);
    }

    //работает
    @GetMapping("/done")
    public List<ADOrderResDto> getDone(@RequestHeader(value = "user-id") Long userId) {
        return userRepService.getDone(userId);
    }

    //работает
    @GetMapping("/history")
    public List<OrderStatusResDto> getHistory(@RequestHeader(value = "user-id") Long userId) {
        return userRepService.getHistory(userId);
    }
}
