package restaurant.endpoint.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import restaurant.endpoint.dto.*;
import restaurant.representation.ProductOrderRepService;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductOrderController {

    private final ProductOrderRepService productOrderRepService;

    @Autowired
    public ProductOrderController(ProductOrderRepService productOrderRepService) {
        this.productOrderRepService = productOrderRepService;
    }

    //работает
    @GetMapping
    public List<ProductOrderResDto> getAll() {
        return productOrderRepService.getAll();
    }

    //работает
    @GetMapping("/{id}")
    public ProductOrderResDto getById(@PathVariable("id") Long id) {
        return productOrderRepService.getById(id);
    }

    //работает
    @GetMapping("/suggest")
    public List<IdNameDto> getAllByStubName(@RequestParam("name") String name) {
        return productOrderRepService.getByName(name);
    }

    //работает
    @PostMapping
    public ProductOrderResDto create(@RequestHeader(value = "user-id") Long userId,
                                     @RequestBody ProductOrderReqDto productOrderReqDto) {
        return productOrderRepService.create(productOrderReqDto, userId);
    }

    //работает
    @DeleteMapping("/{id}")
    public Boolean deleteById(@RequestHeader(value = "user-id") Long userId,
                              @PathVariable("id") Long id) {
        return productOrderRepService.deleteById(id, userId);
    }

    //работает
    @PatchMapping("/{id}")
    public ProductOrderResDto update(@RequestHeader(value = "user-id") Long userId,
                                     @PathVariable("id") Long id,
                                     @RequestBody ProductOrderReqDto productOrderReqDto) {
        return productOrderRepService.update(productOrderReqDto, id, userId);
    }
}
