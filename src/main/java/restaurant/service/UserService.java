package restaurant.service;

import restaurant.endpoint.dto.UserAuthorizationReqDto;
import restaurant.entity.*;

import java.util.List;

public interface UserService {

    List<User> getAll(List<String> role);

    User getById(Long id);

    User getByLogin(String login);

    User create(User newUser, User user);

    Boolean deleteById(Long id, User user);

    User update(User newUser, Long id, User user);

    List<User> getAllByStubName(String name, String surname);

    User authorization(UserAuthorizationReqDto authorizationBody);

    List<CookOrder> getOrdersByStatus(String status, Long userId);

    List<CookOrder> getOrders(Long userId);

    List<ActiveOrder> getActive(Long userId);

    List<DoneOrder> getDone(Long userId);

    List<OrderStatus> getHistory(Long userId);

}
