package restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import restaurant.endpoint.exceptions.ForbiddenException;
import restaurant.endpoint.exceptions.ResourceNotFoundException;
import restaurant.entity.*;
import restaurant.repository.ActiveOrderRepository;
import restaurant.service.ActiveOrderService;
import restaurant.service.CookOrderService;
import restaurant.service.NotifyService;

import java.util.List;

@Service
public class ActiveOrderServiceImpl implements ActiveOrderService {

    private final ActiveOrderRepository activeOrderRepository;
    private final CookOrderService cookOrderService;
    private final NotifyService notifyService;

    @Autowired
    public ActiveOrderServiceImpl(ActiveOrderRepository activeOrderRepository, CookOrderService cookOrderService, NotifyService notifyService) {
        this.activeOrderRepository = activeOrderRepository;
        this.cookOrderService = cookOrderService;

        this.notifyService = notifyService;
    }


    @Override
    @Transactional
    public List<ActiveOrder> getAll() {
        return activeOrderRepository.findAll();
    }

    @Override
    @Transactional
    public ActiveOrder getById(Long id) {
        return activeOrderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    @Transactional
    public ActiveOrder create(ActiveOrder activeOrder, User user) {
        if (user.getStuff().equals(Role.SUPPLIER) || user.getStuff().equals(Role.STOCKMAN) || user.getStuff().equals(Role.ADMIN)) {
            //SUPPLIER работает только с заказами THEN
            if (user.getStuff().equals(Role.SUPPLIER) && activeOrder.getOrder().getType().equals(OrderType.NOW)) {
                throw new ForbiddenException("Вы не можете добавить себе этот заказ");
            }
            //STOCKMAN работает только с заказами NOW
            if (user.getStuff().equals(Role.STOCKMAN) && activeOrder.getOrder().getType().equals(OrderType.THEN)) {
                throw new ForbiddenException("Вы не можете добавить себе этот заказ");
            }
            ActiveOrder targetOrder = ActiveOrder.builder()
                    .order(activeOrder.getOrder())
                    .user(activeOrder.getUser())
                    .build();
            activeOrder.getOrder().setStatus(Status.TAKEN);
            //уведомление пользователя
            notifyService.create(user, activeOrder.getOrder().getUser(), activeOrder.getOrder());

            return activeOrderRepository.save(targetOrder);
        }
        throw new ForbiddenException("Вы не можете выполнить это действие");
    }

    @Override
    @Transactional
    public Boolean deleteById(Long id, User user) {
        if (user.getStuff().equals(Role.SUPPLIER) || user.getStuff().equals(Role.STOCKMAN) || user.getStuff().equals(Role.ADMIN)) {

            //необходимо поменять статус обратно на открытый,
            //чтобы поменять на "сделано" необходимо добавить в список выполненных
            getById(id).getOrder().setStatus(Status.OPEN);
            activeOrderRepository.deleteById(id);
            return true;
        }
        throw new ForbiddenException("Вы не можете выполнить это действие");
    }

    @Override
    public Boolean deleteByOrderId(Long id, User user) {
        if (user.getStuff().equals(Role.SUPPLIER) || user.getStuff().equals(Role.STOCKMAN) || user.getStuff().equals(Role.ADMIN)) {
            ActiveOrder dbOrder = activeOrderRepository.findByOrder(cookOrderService.getById(id));
            activeOrderRepository.deleteById(dbOrder.getId());
            return true;
        }
        throw new ForbiddenException("Вы не можете выполнить это действие");
    }

    @Override
    @Transactional
    public ActiveOrder update(ActiveOrder order, Long id) {
        ActiveOrder dbOrder = getById(id);
        dbOrder.setOrder(order.getOrder());
        dbOrder.setUser(order.getUser());
        return activeOrderRepository.save(dbOrder);
    }


}

