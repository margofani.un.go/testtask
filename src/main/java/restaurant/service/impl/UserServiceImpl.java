package restaurant.service.impl;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import restaurant.endpoint.dto.UserAuthorizationReqDto;
import restaurant.endpoint.exceptions.ForbiddenException;
import restaurant.entity.*;
import restaurant.repository.UserRepository;
import restaurant.service.UserService;
import restaurant.endpoint.exceptions.ResourceNotFoundException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static restaurant.CreateNotEmptyConditionUtils.findInList;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    @Transactional(readOnly = true)
    public List<User> getAll(List<String> role) {
        if (CollectionUtils.isEmpty(role)) {
            return userRepository.findAll();
        } else {
            List<Role> targetRole = role.stream()
                    .map(x -> Role.valueOf(x))
                    .collect(Collectors.toList());
            return userRepository.findAll(((Specification<User>)
                    (root, query, builder) ->
                            findInList(targetRole, () -> root.get("stuff"), builder, query)));
        }

    }

    @Override
    @Transactional(readOnly = true)
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    @Transactional(readOnly = true)
    public User getByLogin(String login) {
        return userRepository.findByLogin(login);
    }


    @Override
    @Transactional
    public User create(User newUser, User user) {
        if (user.getStuff().equals(Role.ADMIN)) {
            User targetUser = User.builder()
                    .surname(newUser.getSurname())
                    .name(newUser.getName())
                    .login(newUser.getLogin())
                    .password(newUser.getPassword())
                    .stuff(newUser.getStuff())
                    .build();
            return userRepository.save(targetUser);
        }
        throw new ForbiddenException("Недостаточно прав");
    }


    @Override
    @Transactional
    public Boolean deleteById(Long id, User user) {
        if (user.getStuff().equals(Role.ADMIN)) {
            userRepository.deleteById(id);
            return true;
        }
        throw new ForbiddenException("Недостаточно прав");
    }

    @Override
    @Transactional
    public User update(User newUser, Long id, User user) {
        if (user.getStuff().equals(Role.ADMIN)) {
            User dbUser = getById(id);
            dbUser.setSurname(newUser.getSurname());
            dbUser.setName(newUser.getName());
            dbUser.setLogin(newUser.getLogin());
            dbUser.setPassword(newUser.getPassword());
            dbUser.setStuff(newUser.getStuff());
            return userRepository.save(dbUser);
        }
        throw new ForbiddenException("Недостаточно прав");
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllByStubName(String name, String surname) {
        return userRepository.findByNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(name, surname);
    }

    @Override
    @Transactional
    public User authorization(UserAuthorizationReqDto authorizationBody) {
        User targetUser = userRepository.findByLogin(authorizationBody.getLogin());
        if (!targetUser.getPassword().equals(authorizationBody.getPassword())) {
            throw new ResourceNotFoundException("Пользователь не найден");
        }
        return targetUser;
    }

    @Override
    @Transactional
    public List<CookOrder> getOrdersByStatus(String status, Long userId) {

        if (Objects.isNull(status)) {
            return getById(userId).getOrders();
        }
        return getById(userId).getOrders()
                .stream()
                .filter(x -> Status.valueOf(status).equals(x.getStatus()))
                .collect(Collectors.toList());

    }

    //для поваров
    @Override
    @Transactional
    public List<CookOrder> getOrders(Long userId) {
        return getById(userId).getOrders()
                .stream()
                .filter(x -> Status.TAKEN.equals(x.getStatus()))
                .collect(Collectors.toList());

    }

    @Override
    @Transactional
    public List<ActiveOrder> getActive(Long userId) {
        return getById(userId).getActiveOrders();
    }

    @Override
    @Transactional
    public List<DoneOrder> getDone(Long userId) {
        return getById(userId).getDoneOrders();
    }

    @Override
    @Transactional
    public List<OrderStatus> getHistory(Long userId) {
        return getById(userId).getHistory();
    }


}
