package restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import restaurant.endpoint.exceptions.ForbiddenException;
import restaurant.endpoint.exceptions.ResourceNotFoundException;
import restaurant.entity.*;
import restaurant.repository.CookOrderRepository;
import restaurant.service.CookOrderService;
import restaurant.service.ProductOrderService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static restaurant.CreateNotEmptyConditionUtils.findInList;

@Service
public class CookOrderServiceImpl implements CookOrderService {

    private final CookOrderRepository cookOrderRepository;
    private final ProductOrderService productOrderService;

    @Autowired
    public CookOrderServiceImpl(CookOrderRepository cookOrderRepository, ProductOrderService productOrderService) {
        this.cookOrderRepository = cookOrderRepository;
        this.productOrderService = productOrderService;
    }


    @Override
    @Transactional(readOnly = true)
    public List<CookOrder> getAll(List<String> status, String type) {

        if (CollectionUtils.isEmpty(status)) {
            if (Objects.isNull(type)) {
                return cookOrderRepository.findAll();
            }
            return cookOrderRepository.findAll()
                    .stream()
                    .filter(x -> OrderType.valueOf(type).equals(x.getType()))
                    .collect(Collectors.toList());

        } else {
            List<Status> targetStatus = status.stream()
                    .map(x -> Status.valueOf(x))
                    .collect(Collectors.toList());
            if (Objects.isNull(type)) {
                return cookOrderRepository.findAll(((Specification<CookOrder>)
                        (root, query, builder) ->
                                findInList(targetStatus, () -> root.get("status"), builder, query)));
            }
            return cookOrderRepository.findAll(((Specification<CookOrder>)
                    (root, query, builder) ->
                            findInList(targetStatus, () -> root.get("status"), builder, query)))
                    .stream()
                    .filter(x -> OrderType.valueOf(type).equals(x.getType()))
                    .collect(Collectors.toList());
        }

    }


    @Override
    @Transactional(readOnly = true)
    public CookOrder getById(Long id) {
        return cookOrderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    @Transactional
    public CookOrder createNow(User user) {
        if (user.getStuff().equals(Role.COOK) || user.getStuff().equals(Role.ADMIN)) {
            CookOrder targetOrder = new CookOrder();
            targetOrder.setType(OrderType.NOW);
            targetOrder.setUser(user);
            targetOrder.setStatus(Status.EMPTY);
            return cookOrderRepository.save(targetOrder);
        }
        throw new ForbiddenException("Вы не можете создавать заказ");

    }

    @Override
    @Transactional
    //это что-то вроде шаблона, ведь в заказе главное список продуктов,
    //и формируется он после заполнения его продуктами
    //однако если есть своя база продуктов и единиц измерения, то это другая история
    //здесь продукты какие напишут
    public CookOrder createThen(User user) {
        if (user.getStuff().equals(Role.COOK) || user.getStuff().equals(Role.ADMIN)) {
            CookOrder targetOrder = new CookOrder();
            targetOrder.setType(OrderType.THEN);
            targetOrder.setUser(user);
            targetOrder.setStatus(Status.EMPTY);
            return cookOrderRepository.save(targetOrder);
        }
        throw new ForbiddenException("Вы не можете создавать заказ");
    }


    @Override
    @Transactional
    public Boolean deleteById(Long id, User user) {
        //удалять может или тот повар, что создал заказ или админ
        if (getById(id).getUser().getId() == user.getId()
                || user.getStuff().equals(Role.ADMIN)) {
            cookOrderRepository.deleteById(id);
            return true;
        }
        throw new ForbiddenException("Вы не можете удалить заказ");

    }

    @Override
    @Transactional
    public CookOrder update(CookOrder order, Long id, User user) {
        //изменять заказ может  повар, который его создал или админ
        if (getById(id).getUser().getId() == user.getId()
                || user.getStuff().equals(Role.ADMIN)) {
            CookOrder dbOrder = getById(id);
            dbOrder.setType(order.getType());
            dbOrder.setUser(order.getUser());
            dbOrder.setNote(order.getNote());
            dbOrder.setProducts(order.getProducts());
            return cookOrderRepository.save(dbOrder);
        }
        throw new ForbiddenException("Вы не можете изменить заказ");
    }

    @Override
    @Transactional
    public CookOrder setStatus(Status status, Long id) {
        CookOrder dbOrder = getById(id);
        dbOrder.setStatus(status);

        return cookOrderRepository.save(dbOrder);
    }

    @Override
    @Transactional
    public CookOrder repeat(CookOrder order, User user) {
        if (user.getStuff().equals(Role.COOK) || user.getStuff().equals(Role.ADMIN)) {
            CookOrder targetOrder = new CookOrder();
            targetOrder.setType(order.getType());
            targetOrder.setUser(user);
            targetOrder.setStatus(Status.EMPTY);
            CookOrder newOrder = cookOrderRepository.save(targetOrder);

            for (ProductOrder x :
                    order.getProducts()) {
                ProductOrder targetProduct = new ProductOrder();
                targetProduct.setOrder(newOrder);
                targetProduct.setProduct(x.getProduct());
                targetProduct.setHowMuch(x.getHowMuch());
                productOrderService.create(targetProduct, user);
            }
            return getById(newOrder.getId());

        }

        throw new ForbiddenException("Вы не можете создать заказ");
    }
}
