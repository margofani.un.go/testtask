package restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import restaurant.endpoint.exceptions.ResourceNotFoundException;
import restaurant.entity.CookOrder;
import restaurant.entity.Notify;
import restaurant.entity.User;
import restaurant.repository.NotifyRepository;
import restaurant.service.NotifyService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class NoifyServiceImpl implements NotifyService {

    private final NotifyRepository notifyRepository;

    @Autowired
    public NoifyServiceImpl(NotifyRepository notifyRepository) {
        this.notifyRepository = notifyRepository;
    }


    @Override
    public List<Notify> getAll(User recipient) {
        if (Objects.isNull(recipient)) {
            return notifyRepository.findAll();
        }
        return notifyRepository.findAll()
                .stream()
                .filter(x -> recipient.getId().equals(x.getRecipient().getId()))
                .collect(Collectors.toList());
    }

    @Override
    public Notify getById(Long id) {
        return notifyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    public Notify create(User performer, User recipient, CookOrder order) {
        Notify targetNotify = Notify.builder()
                .order(order)
                .performer(performer)
                .recipient(recipient)
                .status(false)
                .message("Статус вашего заказа номер " + order.getId() + " был изменен на " + order.getStatus())
                .build();
        return notifyRepository.save(targetNotify);
    }

    @Override
    public Notify update(Notify notify, Long id) {
        Notify dbNotify = getById(id);
        dbNotify.setOrder(notify.getOrder());
        dbNotify.setPerformer(notify.getPerformer());
        dbNotify.setRecipient(notify.getRecipient());
        dbNotify.setStatus(notify.getStatus());
        return notifyRepository.save(dbNotify);
    }

    @Override
    public Boolean deleteById(Long id) {
        notifyRepository.deleteById(id);
        return true;
    }
}
