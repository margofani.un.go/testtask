package restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import restaurant.endpoint.exceptions.ForbiddenException;
import restaurant.endpoint.exceptions.ResourceNotFoundException;
import restaurant.entity.CookOrder;
import restaurant.entity.ProductOrder;
import restaurant.entity.Role;
import restaurant.entity.User;
import restaurant.repository.ProductOrderRepository;
import restaurant.service.CookOrderService;
import restaurant.service.ProductOrderService;

import java.util.List;

@Service
public class ProductOrderServiceImpl implements ProductOrderService {

    private final ProductOrderRepository productOrderRepository;

    @Autowired
    public ProductOrderServiceImpl(ProductOrderRepository productOrderRepository) {
        this.productOrderRepository = productOrderRepository;
    }


    @Override
    @Transactional(readOnly = true)
    public List<ProductOrder> getAll() {
        return productOrderRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public ProductOrder getById(Long id) {
        return productOrderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductOrder> getByStubName(String name) {
        return productOrderRepository.findByProductContainingIgnoreCase(name);
    }

    @Override
    @Transactional
    public ProductOrder create(ProductOrder product, User user) {
        if (user.getStuff().equals(Role.ADMIN) || user.getStuff().equals(Role.COOK)) {
            ProductOrder targetProduct = new ProductOrder();
            targetProduct.setProduct(product.getProduct());
            targetProduct.setHowMuch(product.getHowMuch());
            targetProduct.setOrder(product.getOrder());
            return productOrderRepository.save(targetProduct);
        }
        throw new ForbiddenException("Вы не можете добавлять продукты");

    }

    @Override
    @Transactional
    public ProductOrder update(ProductOrder product, Long id, User user) {
        //менять может либо повар, который добавил его в заказ, либо админ
        if (user.getStuff().equals(Role.ADMIN) ||
                getById(id).getOrder().getUser().getId() == user.getId()) {
            ProductOrder dbProduct = getById(id);
            dbProduct.setProduct(product.getProduct());
            dbProduct.setHowMuch(product.getHowMuch());
            dbProduct.setOrder(product.getOrder());
            return productOrderRepository.save(dbProduct);
        }
        throw new ForbiddenException("Вы не можете изменить продукт");
    }

    @Override
    @Transactional
    public Boolean deleteById(Long id, User user) {
        //удалять может либо повар, который добавил его в заказ, либо админ
        if (user.getStuff().equals(Role.ADMIN) ||
                getById(id).getOrder().getUser().getId() == user.getId()) {
            productOrderRepository.deleteById(id);
            return true;
        }
        throw new ForbiddenException("Вы не можете удалить продукт");
    }
}
