package restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import restaurant.endpoint.exceptions.ForbiddenException;
import restaurant.endpoint.exceptions.ResourceNotFoundException;
import restaurant.entity.*;
import restaurant.repository.DoneOrderRepository;
import restaurant.service.ActiveOrderService;
import restaurant.service.DoneOrderService;
import restaurant.service.NotifyService;

import java.util.List;

@Service
public class DoneOrderServiceImpl implements DoneOrderService {

    private final DoneOrderRepository doneOrderRepository;
    private final ActiveOrderService activeOrderService;
    private final NotifyService notifyService;

    @Autowired
    public DoneOrderServiceImpl(DoneOrderRepository doneOrderRepository, ActiveOrderService activeOrderService, NotifyService notifyService) {
        this.doneOrderRepository = doneOrderRepository;
        this.activeOrderService = activeOrderService;
        this.notifyService = notifyService;
    }

    @Override
    @Transactional
    public List<DoneOrder> getAll() {
        return doneOrderRepository.findAll();
    }

    @Override
    @Transactional
    public DoneOrder getById(Long id) {
        return doneOrderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    @Transactional
    public DoneOrder create(DoneOrder doneOrder, User user) {
        if (user.getStuff().equals(Role.SUPPLIER) || user.getStuff().equals(Role.STOCKMAN) || user.getStuff().equals(Role.ADMIN)) {
            //SUPPLIER работает только с заказами THEN
            if (user.getStuff().equals(Role.SUPPLIER) && doneOrder.getOrder().getType().equals(OrderType.NOW)) {
                throw new ForbiddenException("Вы не можете добавить себе этот заказ");
            }
            //STOCKMAN работает только с заказами NOW
            if (user.getStuff().equals(Role.STOCKMAN) && doneOrder.getOrder().getType().equals(OrderType.THEN)) {
                throw new ForbiddenException("Вы не можете добавить себе этот заказ");
            }

            DoneOrder targetOrder = DoneOrder.builder()
                    .order(doneOrder.getOrder())
                    .user(doneOrder.getUser())
                    .build();
            //меняем статус
            doneOrder.getOrder().setStatus(Status.DONE);
            //удаляем из активных

            activeOrderService.deleteByOrderId(doneOrder.getOrder().getId(), user);//вылетает если не нахдит
            //отправляем уведомление
            notifyService.create(user, doneOrder.getOrder().getUser(), doneOrder.getOrder());

            return doneOrderRepository.save(targetOrder);
        }
        throw new ForbiddenException("Вы не можете выполнить это действие");
    }

    @Override
    @Transactional
    public Boolean deleteById(Long id, User user) {
        if (user.getStuff().equals(Role.SUPPLIER) || user.getStuff().equals(Role.STOCKMAN) || user.getStuff().equals(Role.ADMIN)) {

            ActiveOrder dbOrder = ActiveOrder.builder()
                    .user(getById(id).getUser())
                    .order(getById(id).getOrder())
                    .build();
            doneOrderRepository.deleteById(id);
            notifyService.create(user, getById(id).getOrder().getUser(), getById(id).getOrder());
            activeOrderService.create(dbOrder, user);

            return true;
        }
        throw new ForbiddenException("Вы не можете выполнить это действие");
    }

    @Override
    @Transactional
    public DoneOrder update(DoneOrder order, Long id) {
        DoneOrder dbOrder = getById(id);
        dbOrder.setOrder(order.getOrder());
        dbOrder.setUser(order.getUser());
        return doneOrderRepository.save(dbOrder);
    }
}
