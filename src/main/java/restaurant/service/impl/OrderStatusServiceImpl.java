package restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import restaurant.endpoint.exceptions.ResourceNotFoundException;
import restaurant.entity.OrderStatus;
import restaurant.entity.Status;
import restaurant.repository.OrderStatusRepository;
import restaurant.service.CookOrderService;
import restaurant.service.OrderStatusService;
import restaurant.service.UserService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static restaurant.CreateNotEmptyConditionUtils.findInList;

@Service
public class OrderStatusServiceImpl implements OrderStatusService {

    private final OrderStatusRepository orderStatusRepository;
    private final CookOrderService cookOrderService;
    private final UserService userService;

    @Autowired
    public OrderStatusServiceImpl(OrderStatusRepository orderStatusRepository, CookOrderService cookOrderService, UserService userService) {
        this.orderStatusRepository = orderStatusRepository;
        this.cookOrderService = cookOrderService;
        this.userService = userService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderStatus> getAll(List<String> status) {
        if (CollectionUtils.isEmpty(status)) {
            return orderStatusRepository.findAll();
        } else {
            List<Status> targetStatus = status.stream()
                    .map(x -> Status.valueOf(x))
                    .collect(Collectors.toList());
            return orderStatusRepository.findAll(((Specification<OrderStatus>)
                    (root, query, builder) ->
                            findInList(targetStatus, () -> root.get("status"), builder, query)));
        }

    }

    @Override
    @Transactional(readOnly = true)
    public OrderStatus getById(Long id) {
        return orderStatusRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Not found id = %d", id)));
    }

    @Override
    @Transactional
    public OrderStatus create(OrderStatus status) {
        OrderStatus targetStatus = new OrderStatus();
        targetStatus.setOrder(status.getOrder());
        targetStatus.setStatus(status.getStatus());
        targetStatus.setUser(status.getUser());
        targetStatus.setTime(LocalDateTime.now());
        return orderStatusRepository.save(targetStatus);
    }

    @Override
    @Transactional
    public OrderStatus update(OrderStatus status, Long id) {
        OrderStatus dbStatus = getById(id);
        dbStatus.setUser(status.getUser());
        dbStatus.setStatus(status.getStatus());
        dbStatus.setUser(status.getUser());
        dbStatus.setTime(status.getTime());
        return null;
    }

    @Override
    @Transactional
    public Boolean deleteById(Long id) {
        orderStatusRepository.deleteById(id);
        return true;
    }
}
