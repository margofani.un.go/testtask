package restaurant.service;

import restaurant.entity.ActiveOrder;

import restaurant.entity.CookOrder;
import restaurant.entity.User;

import java.util.List;

public interface ActiveOrderService {

    List<ActiveOrder> getAll();

    ActiveOrder getById(Long id);

    ActiveOrder create(ActiveOrder activeOrder, User user);

    Boolean deleteById(Long id, User user);

    Boolean deleteByOrderId(Long id, User user);

    ActiveOrder update(ActiveOrder order, Long id);
}
