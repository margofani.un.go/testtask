package restaurant.service;

import restaurant.entity.CookOrder;
import restaurant.entity.Notify;
import restaurant.entity.User;

import java.util.List;

public interface NotifyService {
    //может быть фильтр по id заказа?
    List<Notify> getAll(User recipient);

    Notify getById(Long id);

    Notify create(User performer, User recipient, CookOrder order);

    Notify update(Notify notify, Long id);

    Boolean deleteById(Long id);
}
