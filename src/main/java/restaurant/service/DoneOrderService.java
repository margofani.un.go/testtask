package restaurant.service;

import restaurant.entity.CookOrder;
import restaurant.entity.DoneOrder;
import restaurant.entity.User;

import java.util.List;

public interface DoneOrderService {
    List<DoneOrder> getAll();

    DoneOrder getById(Long id);

    DoneOrder create(DoneOrder doneOrder, User user);

    Boolean deleteById(Long id, User user);

    DoneOrder update(DoneOrder order, Long id);
}
