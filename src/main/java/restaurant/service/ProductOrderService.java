package restaurant.service;

import restaurant.entity.ProductOrder;
import restaurant.entity.User;

import java.util.List;

public interface ProductOrderService {

    List<ProductOrder> getAll();

    ProductOrder getById(Long id);

    List<ProductOrder> getByStubName(String name);

    ProductOrder create(ProductOrder product, User user);

    ProductOrder update(ProductOrder product, Long id, User user);

    Boolean deleteById(Long id, User user);
}
