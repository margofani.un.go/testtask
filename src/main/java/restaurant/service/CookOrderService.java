package restaurant.service;

import restaurant.entity.CookOrder;
import restaurant.entity.ProductOrder;
import restaurant.entity.Status;
import restaurant.entity.User;

import java.util.List;

public interface CookOrderService {

    List<CookOrder> getAll(List<String> status, String type);

    CookOrder getById(Long id);

    CookOrder createNow(User user);

    CookOrder createThen(User user);

    Boolean deleteById(Long id, User user);

    CookOrder update(CookOrder order, Long id, User user);

    CookOrder setStatus(Status status, Long id);

    CookOrder repeat(CookOrder order, User user);
}
