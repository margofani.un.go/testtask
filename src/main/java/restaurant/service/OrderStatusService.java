package restaurant.service;

import restaurant.entity.OrderStatus;
import restaurant.entity.Status;

import java.util.List;

public interface OrderStatusService {

    List<OrderStatus> getAll(List<String> status);

    OrderStatus getById(Long id);

    OrderStatus create(OrderStatus status);

    OrderStatus update(OrderStatus status, Long id);

    Boolean deleteById(Long id);

}
