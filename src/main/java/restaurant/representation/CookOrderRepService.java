package restaurant.representation;

import restaurant.endpoint.dto.CookOrderReqDto;
import restaurant.endpoint.dto.CookOrderResDto;
import restaurant.entity.Status;

import java.util.List;

public interface CookOrderRepService {

    List<CookOrderResDto> getAll(List<String> status, String type);

    CookOrderResDto getById(Long id);

    CookOrderResDto createNow(Long userId);

    CookOrderResDto createThen(Long userId);

    CookOrderResDto update(CookOrderReqDto orderReqDto, Long id, Long userId);

    Boolean deleteById(Long id, Long userId);

    CookOrderResDto repeat(Long id, Long userI);

    CookOrderResDto setNewStatus(Long id, Status status);

    // CookOrderResDto setStatusOpen(Long id);

    // CookOrderResDto setStatusTaken(Long id);

    // CookOrderResDto setStatusDone(Long id);
}
