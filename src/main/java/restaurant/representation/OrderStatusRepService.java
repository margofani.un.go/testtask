package restaurant.representation;

import restaurant.endpoint.dto.OrderStatusReqDto;
import restaurant.endpoint.dto.OrderStatusResDto;
import restaurant.entity.OrderStatus;

import java.util.List;

public interface OrderStatusRepService {

    List<OrderStatusResDto> getAll(List<String> status);

    OrderStatusResDto getById(Long id);

    OrderStatusResDto create(OrderStatusReqDto orderStatusReqDto);

    OrderStatusResDto update(OrderStatusReqDto orderStatusReqDto, Long id);

    Boolean deleteById(Long id);

    OrderStatusResDto toDto(OrderStatus status);

}
