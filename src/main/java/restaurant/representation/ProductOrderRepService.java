package restaurant.representation;

import restaurant.endpoint.dto.*;

import java.util.List;

public interface ProductOrderRepService {

    List<ProductOrderResDto> getAll();

    ProductOrderResDto getById(Long id);

    List<IdNameDto> getByName(String name);

    ProductOrderResDto create(ProductOrderReqDto product, Long userId);

    ProductOrderResDto update(ProductOrderReqDto product, Long id, Long userId);

    Boolean deleteById(Long id, Long userId);

}
