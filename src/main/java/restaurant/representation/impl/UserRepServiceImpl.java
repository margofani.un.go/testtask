package restaurant.representation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import restaurant.endpoint.dto.*;
import restaurant.entity.*;
import restaurant.representation.OrderStatusRepService;
import restaurant.representation.UserRepService;
import restaurant.service.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserRepServiceImpl implements UserRepService {

    private final UserService userService;
    private final ActiveOrderService activeOrderService;
    private final DoneOrderService doneOrderService;
    private final CookOrderService cookOrderService;
    private final NotifyService notifyService;
    private final OrderStatusRepService orderStatusRepService;

    @Autowired
    public UserRepServiceImpl(UserService userService, ActiveOrderService activeOrderService, DoneOrderService doneOrderService, CookOrderService cookOrderService, NotifyService notifyService, OrderStatusRepService orderStatusRepService) {
        this.userService = userService;
        this.activeOrderService = activeOrderService;
        this.doneOrderService = doneOrderService;
        this.cookOrderService = cookOrderService;
        this.notifyService = notifyService;
        this.orderStatusRepService = orderStatusRepService;
    }

    @Override
    public List<IdNameDto> getAll(List<String> role) {
        return userService.getAll(role)
                .stream()
                .map(this::toIdNameDto).collect(Collectors.toList());
    }

    @Override
    public UserResDto getById(Long id) {
        return toDto(userService.getById(id));
    }

    @Override
    public List<IdNameDto> getAllByName(String name, String surname) {
        return userService.getAllByStubName(name, surname)
                .stream()
                .map(this::toIdNameDto).collect(Collectors.toList());
    }

    @Override
    public UserResDto getByLogin(String login) {
        return toDto(userService.getByLogin(login));
    }


    @Override
    public UserResDto create(UserReqDto userReqDto, Long userId) {
        return toDto(userService.create(toEntity(userReqDto), userService.getById(userId)));
    }


    @Override
    public UserResDto update(UserReqDto userReqDto, Long id, Long userId) {
        return toDto(userService.update(toEntity(userReqDto), id, userService.getById(userId)));
    }

    @Override
    public Boolean deleteById(Long id, Long userId) {
        return userService.deleteById(id, userService.getById(userId));
    }

    @Override
    public UserResDto authorization(UserAuthorizationReqDto authorizationBody) {
        return toDto(userService.authorization(authorizationBody));
    }

    @Override
    public List<CookOrderResDto> getOrdersByStatus(String status, Long userId) {
        return userService.getOrdersByStatus(status, userId).stream()
                .map(this::orderToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CookOrderResDto> getOrders(Long userId) {
        return userService.getOrders(userId).stream()
                .map(this::orderToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ADOrderResDto addActive(ADOrderReqDto adOrderDto, Long userId) {
        return activeToDto(activeOrderService.create(activeToEntity(adOrderDto), userService.getById(userId)));
    }

    @Override
    public Boolean deleteActive(Long id, Long userId) {
        return activeOrderService.deleteById(id, userService.getById(userId));
    }

    @Override
    public ADOrderResDto addDone(ADOrderReqDto adOrderDto, Long userId) {
        return doneToDto(doneOrderService.create(doneToEntity(adOrderDto), userService.getById(userId)));
    }

    @Override
    public Boolean deleteDone(Long id, Long userId) {
        return doneOrderService.deleteById(id, userService.getById(userId));
    }

    @Override
    public List<NotifyResDto> getNotify(Long recipientId) {
        return notifyService.getAll(userService.getById(recipientId)).stream()
                .map(this::notifyToDto)
                .collect(Collectors.toList());
    }

    @Override
    public NotifyResDto getNotifyById(Long id) {
        return notifyToDto(notifyService.getById(id));
    }

    @Override
    public Boolean setNotifyStatus(Long notifyId) {
        Notify targetNotify = notifyService.getById(notifyId);
        targetNotify.setStatus(true);
        notifyService.update(targetNotify, notifyId);

        return true;
    }

    @Override
    public List<ADOrderResDto> getActive(Long userId) {
        return userService.getActive(userId).stream()
                .map(this::activeToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ADOrderResDto> getDone(Long userId) {
        return userService.getDone(userId).stream()
                .map(this::doneToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<OrderStatusResDto> getHistory(Long userId) {
        return userService.getHistory(userId).stream()
                .map(x -> orderStatusRepService.toDto(x))
                .collect(Collectors.toList());
    }

    private IdNameDto toIdNameDto(User user) {
        return IdNameDto.builder()
                .name(user.getName() + " " + user.getSurname())
                .id(user.getId())
                .build();
    }


    private NotifyResDto notifyToDto(Notify notify) {
        return NotifyResDto.builder()
                .id(notify.getId())
                .orderId(notify.getOrder().getId())
                .performer(toIdNameDto(notify.getPerformer()))
                .recipient(toIdNameDto(notify.getRecipient()))
                .status(notify.getStatus())
                .message(notify.getMessage())
                .build();
    }

    private UserResDto toDto(User user) {
        return UserResDto.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .login(user.getLogin())
                .password(user.getPassword())
                .orders(!CollectionUtils.isEmpty(user.getOrders()) ?
                        user.getOrders()
                                .stream()
                                .map(x -> x.getId())
                                .collect(Collectors.toList())
                        : Collections.emptyList())
                .stuff(user.getStuff())
                .activeOrders(!CollectionUtils.isEmpty(user.getActiveOrders()) ?
                        user.getActiveOrders()
                                .stream()
                                .map(x -> x.getId())
                                .collect(Collectors.toList())
                        : Collections.emptyList())
                .doneOrders(!CollectionUtils.isEmpty(user.getDoneOrders()) ?
                        user.getDoneOrders()
                                .stream()
                                .map(x -> x.getId())
                                .collect(Collectors.toList())
                        : Collections.emptyList())
                .build();


    }

    private User toEntity(UserReqDto userReqDto) {
        return User.builder()
                .name(userReqDto.getName())
                .surname(userReqDto.getSurname())
                .login(userReqDto.getLogin())
                .password(userReqDto.getPassword())
                .stuff(Role.valueOf(userReqDto.getStuff()))
                .build();
    }

    private ActiveOrder activeToEntity(ADOrderReqDto adOrderDto) {
        return ActiveOrder.builder()
                .order(cookOrderService.getById(adOrderDto.getOrderId()))
                .user(userService.getById(adOrderDto.getUserId()))
                .build();
    }


    private DoneOrder doneToEntity(ADOrderReqDto adOrderDto) {
        return DoneOrder.builder()
                .order(cookOrderService.getById(adOrderDto.getOrderId()))
                .user(userService.getById(adOrderDto.getUserId()))
                .build();
    }

    private ADOrderResDto activeToDto(ActiveOrder activeOrder) {
        return ADOrderResDto.builder()
                .id(activeOrder.getId())
                .user(IdNameDto.builder()
                        .id(activeOrder.getUser().getId())
                        .name(activeOrder.getUser().getName())
                        .build())
                .orderId(activeOrder.getOrder().getId())
                .build();
    }

    private ADOrderResDto doneToDto(DoneOrder doneOrder) {
        return ADOrderResDto.builder()
                .id(doneOrder.getId())
                .user(IdNameDto.builder()
                        .id(doneOrder.getUser().getId())
                        .name(doneOrder.getUser().getName())
                        .build())
                .orderId(doneOrder.getOrder().getId())
                .build();
    }

    private CookOrderResDto orderToDto(CookOrder order) {
        return CookOrderResDto.builder()
                .id(order.getId())
                .type(order.getType())
                .note(order.getNote())
                .user(IdNameDto.builder()
                        .id(order.getUser().getId())
                        .name(order.getUser().getName())
                        .build())
                .products(!CollectionUtils.isEmpty(order.getProducts()) ?
                        order.getProducts()
                                .stream()
                                .map(product -> product.getProduct() + " " + product.getHowMuch()).collect(Collectors.toList())
                        : Collections.emptyList())
                .status(order.getStatus())
                .build();

    }
}
