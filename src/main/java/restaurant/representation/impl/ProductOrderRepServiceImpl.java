package restaurant.representation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import restaurant.endpoint.dto.*;
import restaurant.entity.ProductOrder;
import restaurant.representation.ProductOrderRepService;
import restaurant.service.CookOrderService;
import restaurant.service.ProductOrderService;
import restaurant.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductOrderRepServiceImpl implements ProductOrderRepService {

    private final ProductOrderService productOrderService;
    private final CookOrderService cookOrderService;
    private final UserService userService;

    @Autowired
    public ProductOrderRepServiceImpl(ProductOrderService productOrderService, CookOrderService cookOrderService, UserService userService) {
        this.productOrderService = productOrderService;
        this.cookOrderService = cookOrderService;
        this.userService = userService;
    }

    @Override
    public List<ProductOrderResDto> getAll() {
        return productOrderService.getAll()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductOrderResDto getById(Long id) {
        return toDto(productOrderService.getById(id));
    }

    @Override
    public List<IdNameDto> getByName(String name) {
        return productOrderService.getByStubName(name)
                .stream()
                .map(this::toIdNameDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductOrderResDto create(ProductOrderReqDto product, Long userId) {

        return toDto(productOrderService.create(toEntity(product), userService.getById(userId)));
    }

    @Override
    public ProductOrderResDto update(ProductOrderReqDto product, Long id, Long userId) {
        return toDto(productOrderService.update(toEntity(product), id, userService.getById(userId)));
    }

    @Override
    public Boolean deleteById(Long id, Long userId) {
        return productOrderService.deleteById(id, userService.getById(userId));
    }

    private IdNameDto toIdNameDto(ProductOrder productOrder) {
        return IdNameDto.builder().id(productOrder.getId()).name(productOrder.getProduct()).build();
    }

    private ProductOrderResDto toDto(ProductOrder productOrder) {
        return ProductOrderResDto.builder()
                .id(productOrder.getId())
                .product(productOrder.getProduct())
                .howMuch(productOrder.getHowMuch())
                .order(productOrder.getOrder().getId())
                .build();
    }

    private ProductOrder toEntity(ProductOrderReqDto productOrderReqDto) {
        return ProductOrder.builder()
                .product(productOrderReqDto.getProduct())
                .howMuch(productOrderReqDto.getHowMuch())
                .order(cookOrderService.getById(productOrderReqDto.getOrder()))
                .build();
    }


}
