package restaurant.representation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import restaurant.endpoint.dto.IdNameDto;
import restaurant.endpoint.dto.OrderStatusReqDto;
import restaurant.endpoint.dto.OrderStatusResDto;
import restaurant.entity.OrderStatus;
import restaurant.entity.Status;
import restaurant.representation.OrderStatusRepService;
import restaurant.service.CookOrderService;
import restaurant.service.OrderStatusService;
import restaurant.service.UserService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class OrderStatusRepServiceImpl implements OrderStatusRepService {

    private final OrderStatusService orderStatusService;
    private final UserService userService;
    private final CookOrderService cookOrderService;

    @Autowired
    public OrderStatusRepServiceImpl(OrderStatusService orderStatusService, UserService userService, CookOrderService cookOrderService) {
        this.orderStatusService = orderStatusService;
        this.userService = userService;
        this.cookOrderService = cookOrderService;
    }

    @Override
    public List<OrderStatusResDto> getAll(List<String> status) {
        return orderStatusService.getAll(status).stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public OrderStatusResDto getById(Long id) {
        return toDto(orderStatusService.getById(id));
    }

    @Override
    public OrderStatusResDto create(OrderStatusReqDto orderStatusReqDto) {
        return toDto(orderStatusService.create(toEntity(orderStatusReqDto)));
    }

    @Override
    public OrderStatusResDto update(OrderStatusReqDto orderStatusReqDto, Long id) {
        return toDto(orderStatusService.update(toEntity(orderStatusReqDto), id));
    }

    @Override
    public Boolean deleteById(Long id) {

        return orderStatusService.deleteById(id);
    }


    public OrderStatusResDto toDto(OrderStatus status) {
        return OrderStatusResDto.builder()
                .id(status.getId())
                .user(IdNameDto.builder()
                        .id(status.getUser().getId())
                        .name(status.getUser().getName() + " " + status.getUser().getSurname())
                        .build())
                .status(status.getStatus())
                .order(status.getOrder().getId())
                .time(status.getTime())
                .build();
    }

    private OrderStatus toEntity(OrderStatusReqDto status) {
        return OrderStatus.builder()
                .order(cookOrderService.getById(status.getOrder()))
                .user(userService.getById(status.getUser()))
                .status(Status.valueOf(status.getStatus()))
                .build();
    }


}
