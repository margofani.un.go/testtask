package restaurant.representation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import restaurant.endpoint.dto.*;
import restaurant.entity.CookOrder;
import restaurant.entity.OrderType;
import restaurant.entity.Status;
import restaurant.entity.User;
import restaurant.representation.CookOrderRepService;
import restaurant.service.CookOrderService;
import restaurant.service.ProductOrderService;
import restaurant.service.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CookOrderRepServiceImpl implements CookOrderRepService {

    private final CookOrderService cookOrderService;
    private final UserService userService;
    private final ProductOrderService productService;

    @Autowired
    public CookOrderRepServiceImpl(CookOrderService cookOrderService, UserService userService, ProductOrderService productService) {
        this.cookOrderService = cookOrderService;
        this.userService = userService;
        this.productService = productService;
    }


    @Override
    public List<CookOrderResDto> getAll(List<String> status, String type) {
        return cookOrderService.getAll(status, type)
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public CookOrderResDto getById(Long id) {
        return toDto(cookOrderService.getById(id));
    }

    @Override
    public CookOrderResDto createNow(Long userId) {
        return toDto(cookOrderService.createNow(userService.getById(userId)));
    }

    @Override
    public CookOrderResDto createThen(Long userId) {
        return toDto(cookOrderService.createThen(userService.getById(userId)));
    }

    @Override
    public CookOrderResDto update(CookOrderReqDto orderReqDto, Long id, Long userId) {
        return toDto(cookOrderService.update(toEntity(orderReqDto), id, userService.getById(userId)));
    }

    @Override
    public Boolean deleteById(Long id, Long userId) {
        cookOrderService.deleteById(id, userService.getById(userId));
        return true;
    }

    @Override
    public CookOrderResDto repeat(Long id, Long userId) {
        return toDto(cookOrderService.repeat(cookOrderService.getById(id), userService.getById(userId)));
    }

    @Override
    public CookOrderResDto setNewStatus(Long id, Status status) {
        return toDto(cookOrderService.setStatus(status, id));
    }

    private CookOrderResDto toDto(CookOrder order) {
        return CookOrderResDto.builder()
                .id(order.getId())
                .type(order.getType())
                .note(order.getNote())
                .user(IdNameDto.builder()
                        .id(order.getUser().getId())
                        .name(order.getUser().getName())
                        .build())
                .products(!CollectionUtils.isEmpty(order.getProducts()) ?
                        order.getProducts()
                                .stream()
                                .map(product -> product.getProduct() + " " + product.getHowMuch()).collect(Collectors.toList())
                        : Collections.emptyList())
                .status(order.getStatus())
                .build();

    }

    private CookOrder toEntity(CookOrderReqDto cookOrderReqDto) {
        return CookOrder.builder()
                .user(userService.getById(cookOrderReqDto.getUser()))
                .note(cookOrderReqDto.getNote())
                .type(OrderType.valueOf(cookOrderReqDto.getType()))
                .status(Status.valueOf(cookOrderReqDto.getStatus()))
                .build();
    }

}
