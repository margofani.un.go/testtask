package restaurant.representation;

import restaurant.endpoint.dto.*;
import restaurant.entity.ActiveOrder;
import restaurant.entity.DoneOrder;
import restaurant.entity.OrderStatus;

import java.util.List;

public interface UserRepService {

    List<IdNameDto> getAll(List<String> role);

    UserResDto getById(Long id);

    List<IdNameDto> getAllByName(String name, String surname);

    UserResDto getByLogin(String login);

    UserResDto create(UserReqDto userReqDto, Long userId);

    UserResDto update(UserReqDto userReqDto, Long id, Long userId);

    Boolean deleteById(Long id, Long userId);

    UserResDto authorization(UserAuthorizationReqDto authorizationBody);

    List<CookOrderResDto> getOrdersByStatus(String status, Long userId);

    List<CookOrderResDto> getOrders(Long userId);

    ADOrderResDto addActive(ADOrderReqDto adOrderDto, Long userId);

    Boolean deleteActive(Long id, Long userId);

    ADOrderResDto addDone(ADOrderReqDto adOrderDto, Long userId);

    Boolean deleteDone(Long id, Long userId);

    List<NotifyResDto> getNotify(Long recipientId);

    NotifyResDto getNotifyById(Long id);

    Boolean setNotifyStatus(Long notifyId);

    List<ADOrderResDto> getActive(Long userId);

    List<ADOrderResDto> getDone(Long userId);

    List<OrderStatusResDto> getHistory(Long userId);
}
