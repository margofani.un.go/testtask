package restaurant;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.function.Supplier;

public class CreateNotEmptyConditionUtils {
    /**
     * Предикат, позволяющий комбинировать условия поиска в коллекциях элементов
     *
     * @param elements     список элементов, для которых будет сформировано IN query <br/>
     *                     в случае, если входящий список null или empty, условие будет исключено при построении запроса
     *                     и заменено с помощью query вида {@code WHERE 1=1}
     * @param pathSupplier путь к элементу, который необходимо поискать в списке
     * @param builder      вспомогательный параметр, генерирующий условие выборки вида {@code WHERE 1=1}
     * @param query
     * @return условие для запроса поиска параметра во входящем списке элементов
     */
    public static Predicate findInList(List<?> elements,
                                       Supplier<Path<?>> pathSupplier,
                                       CriteriaBuilder builder,
                                       CriteriaQuery<?> query) {
        query.distinct(true);
        return CollectionUtils.isEmpty(elements)
                ? builder.conjunction()
                : pathSupplier.get().in(elements);
    }

    public static Predicate findInField(Object element,
                                        Supplier<Path<?>> pathSupplier,
                                        CriteriaBuilder builder,
                                        CriteriaQuery<?> query) {
        query.distinct(true);
        return StringUtils.isEmpty(element)
                ? builder.conjunction()
                : pathSupplier.get().in(element);

    }
}
