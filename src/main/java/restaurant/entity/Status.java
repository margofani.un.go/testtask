package restaurant.entity;

public enum Status {
    EMPTY, OPEN, TAKEN, DONE
}
