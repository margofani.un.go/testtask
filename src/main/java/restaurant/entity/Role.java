package restaurant.entity;

public enum Role {
    ADMIN, COOK, STOCKMAN, SUPPLIER
}
