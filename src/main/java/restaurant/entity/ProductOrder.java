package restaurant.entity;

import lombok.*;

import javax.persistence.*;

@Table(name = "product_order")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product")
    private String product;
    @Column(name = "how_much")
    private String howMuch;
    @ManyToOne
    @JoinColumn(name = "order_id")
    private CookOrder order;
}
