package restaurant.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Table(name = "cook_order")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CookOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "type")
    private OrderType type;
    @OneToMany(mappedBy = "order")
    private List<ProductOrder> products;
    @Column(name = "note")
    private String note;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "status")
    private Status status;
    @OneToMany(mappedBy = "order")
    private List<OrderStatus> history;
    @OneToMany(mappedBy = "order")
    private List<ActiveOrder> activeOrders;
    @OneToMany(mappedBy = "order")
    private List<DoneOrder> doneOrders;
    @OneToMany(mappedBy = "order")
    private List<Notify> notifies;


}
