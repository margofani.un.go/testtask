package restaurant.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Table(name = "user")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "surname")
    private String surname;
    @Column(name = "name")
    private String name;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    @Column (name= "stuff")
    private Role stuff;
    @OneToMany(mappedBy = "user")
    private List<CookOrder> orders;
    @OneToMany(mappedBy = "user")
    private List<OrderStatus> history;
    @OneToMany(mappedBy = "user")
    private List<ActiveOrder> activeOrders;
    @OneToMany(mappedBy = "user")
    private List<DoneOrder> doneOrders;
    @OneToMany(mappedBy = "recipient")
    private List<Notify> notifyGet;
    @OneToMany(mappedBy = "performer")
    private List<Notify> notifySend;

}
