package restaurant.entity;

import lombok.*;

import javax.persistence.*;

@Table(name = "notification")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Notify {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //кому
    @ManyToOne
    @JoinColumn(name = "recipient")
    private User recipient;
    //кто
    @ManyToOne
    @JoinColumn(name = "performer")
    private User performer;
    //какой заказ, оттуда и вытягивается стаус заказа
    @ManyToOne
    @JoinColumn(name = "order_i")
    private CookOrder order;
    //true - прочитано
    //false - не прочитано, это по дефолту
    @Column(name = "status")
    private Boolean status;
    @Column(name = "message")
    private String message;
}
