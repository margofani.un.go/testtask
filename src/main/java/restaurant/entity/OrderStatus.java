package restaurant.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDateTime;

@Table(name = "order_status")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "order_id")
    private CookOrder order;
    @Column(name = "status")
    private Status status;
    @ManyToOne
    @JoinColumn(name = "user_id")
    //who
    private User user;
    @Column(name = "time")
    //when
    private LocalDateTime time;

}
