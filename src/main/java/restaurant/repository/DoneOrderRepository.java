package restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import restaurant.entity.DoneOrder;

@Repository
public interface DoneOrderRepository extends JpaRepository<DoneOrder, Long>, JpaSpecificationExecutor<DoneOrder> {
}
