package restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import restaurant.entity.CookOrder;

import java.util.List;

@Repository
public interface CookOrderRepository extends JpaRepository<CookOrder, Long>, JpaSpecificationExecutor<CookOrder> {


}
