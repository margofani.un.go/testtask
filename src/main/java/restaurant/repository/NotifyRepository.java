package restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import restaurant.entity.Notify;

@Repository
public interface NotifyRepository extends JpaRepository<Notify, Long>, JpaSpecificationExecutor<Notify> {
}
