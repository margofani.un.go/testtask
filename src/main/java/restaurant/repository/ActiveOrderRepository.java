package restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import restaurant.entity.ActiveOrder;
import restaurant.entity.CookOrder;

@Repository
public interface ActiveOrderRepository extends JpaRepository<ActiveOrder, Long>, JpaSpecificationExecutor<ActiveOrder> {

    ActiveOrder findByOrder(CookOrder order);
}
